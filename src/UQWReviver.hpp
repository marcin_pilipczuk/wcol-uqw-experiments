#pragma once

#include "Headers.hpp"
#include "ReadTxt.hpp"
#include "FilesOps.hpp"
#include "ComputeWReach.hpp"

vector<int> ReviveRedundantForb(vector<vector<int>>& graph, int R, vector<int> forb, vector<int>& scat);

