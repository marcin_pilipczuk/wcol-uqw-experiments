Based on the code by Christoph Dittmann.

apprtTW implements the min-fill-in heuristic to compute an elimination order for treewidth. In this version,
it first takes a vertex with maximum degree as trhe last one in the order and then orders vertices from the
biggest to the least one. In an iteration, assume that positions >i already have vertices. Among all unsorted
vertices, it takes one (with the least id, or randomly if parameter -r is given to the programme) with the least
back degree. The back degree of a vertex is the number of yet unsorted vertices reachable by paths that use
only sorted vertices (except the first and the last ones on the path).


------------
INSTALLATION
------------

You need boost's program options and disjoint sets to compile apprTW. Please, set an environment variable BOOST to the boost path, e.g.

  export BOOST=/usr/local/boost_1_66_0/

and then enter

  make

which installs apprTW to ./bin.


-----
USAGE
-----
Try ./apprTW -h.
The input file should contain a list of edges written line by line. An edge is a pair of integers separated by white
spaces. Lines starting with #, % etc. are ignored. Misformatted lines are ignored. In general, reading input is
very robust.

If you just want to print the computed order, do

  ./apprTW -i /path/to/GraphFile.txtg -o

or

  ./apprTW -i /path/to/GraphFile.txtg -o -r

The order is a list of vertex identifires separated by a space.

On large single files it may make sense to show the progress (-e) and print the time used for computations (-t).
In the time output, the time for reading the graph is not taken into account.

The test implemets a simple depth-first search and runs in quadratic time.

Details (-d <level>) with level > 1 only make sense for debugging. If you want to print some information except
the order or the width, you probably want to set the details level to 1.


Parameter -r (see the explanation above) sometimes gives slightly better orders (of smaller width).
